//
//  LintWorker.swift
//  ThirdCourceWork
//
//  Created by Семен Соколов
//

import Foundation
import Yams

struct RulesFile: Codable {
    var custom_rules: [String: RuleMin]
}

struct LintWorker {
    
    var pathToProj = URL(string: "")
    
    func makeLintFile(completion: @escaping ([String]) -> Void) {
        guard let pathToSwiftLintFile = pathToProj?.path,
              let pathToProj = pathToProj?.deletingLastPathComponent().path else { return }
        
        let process = Process()
        let swiftlintPath = "/usr/local/bin/swiftlint"
        let projectPath = pathToProj
        let configPath = pathToSwiftLintFile
        
        process.currentDirectoryPath = projectPath
        process.launchPath = swiftlintPath
        process.arguments = ["lint", "--config", configPath, "--path", projectPath]
        
        let outputPipe = Pipe()
        process.standardOutput = outputPipe
        process.standardError = outputPipe
        
        process.launch()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            process.terminate()
        }
        
        var output = ""
        
        let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
        if let outputString = String(data: outputData, encoding: .utf8) {
            output = outputString
        }
        
        let arrayOfWarnings: [String] = output.components(separatedBy: "\n")
        completion(arrayOfWarnings)
    }
    
    
    mutating func createSwiftlintConfigFile(atPath path: String) {
        let fileManager = FileManager.default
        let configFileName = ".swiftlint.yml"
        let configURL = URL(fileURLWithPath: path).deletingLastPathComponent().appendingPathComponent(configFileName)
        pathToProj = configURL
        
        if fileManager.fileExists(atPath: configURL.path) {
            print("Файл .swiftlint.yml уже существует.")
            return
        }
        
        let configFileContent = ""
        
        do {
            try configFileContent.write(to: configURL, atomically: true, encoding: .utf8)
            print("Файл .swiftlint.yml успешно создан.")
        } catch {
            print("Ошибка при создании файла .swiftlint.yml: \(error.localizedDescription)")
        }
    }
    
    func getAllRules() -> [RuleMin] {
        guard let path = pathToProj?.path else { return [] }
        do {
            var rulesFile = RulesFile(custom_rules: [:])
            
            if FileManager.default.fileExists(atPath: path) {
                let fileContents = try String(contentsOfFile: path, encoding: .utf8)
                
                if !fileContents.isEmpty && !(fileContents == "\n") {
                    let decoder = YAMLDecoder()
                    rulesFile = try decoder.decode(RulesFile.self, from: fileContents)
                }
            }
            
            let rules = rulesFile.custom_rules.values.map { $0 }
            
            return rules
        } catch {
            print("ошибка \(error)")
            return []
        }
    }
    
    func addRule(rule: Rule, rulemin: RuleMin) {
        guard let path = pathToProj?.path else { return }
        
        do {
            var rulesFile = RulesFile(custom_rules: [:])
            
            if FileManager.default.fileExists(atPath: path) {
                let fileContents = try String(contentsOfFile: path, encoding: .utf8)
                
                if !fileContents.isEmpty && !(fileContents == "\n") {
                    let decoder = YAMLDecoder()
                    rulesFile = try decoder.decode(RulesFile.self, from: fileContents)
                }
            }
            
            rulesFile.custom_rules[rule.id] = rulemin
            
            let encoder = YAMLEncoder()
            let yamlString = try encoder.encode(rulesFile)
            
            try yamlString.write(toFile: path, atomically: true, encoding: .utf8)
        } catch {
            print("Ошибка: \(error)")
        }
    }
    
    
    func getProgectFolder(path: String) -> String {
        let fileManager = FileManager.default
        
        var currentPath = path
        while !currentPath.isEmpty {
            currentPath = (currentPath as NSString).deletingLastPathComponent
            let files = try? fileManager.contentsOfDirectory(atPath: currentPath)
            
            guard let files = files else { return "" }
            
            for file in files {
                if file.hasSuffix(".plist") && file.hasPrefix("info") {
                    return readWorkspacePathFromInfoPlist(atPath: currentPath)
                }
            }
        }
        return ""
    }
    
    private func readWorkspacePathFromInfoPlist(atPath path: String) -> String {
        guard let infoPlistPath = Bundle(path: path)?.path(forResource: "info", ofType: "plist"),
              let infoDict = NSDictionary(contentsOfFile: infoPlistPath),
              let workspacePath = infoDict["WorkspacePath"] as? String else {
            return ""
        }
        print(workspacePath)
        return workspacePath
    }
}
