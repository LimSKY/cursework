//
//  LintWorkerViewController.swift
//  ThirdCourceWork
//
//  Created by Семен Соколов
//

import Cocoa

final class LintWorkerViewController: NSViewController {
    var dataSource: XcodeDatabase
    private var lintWorker: LintWorker = LintWorker()
    private var pathToProject = ""
    
    private var rulesDataSource: [RuleMin] = []
    
    // UI
    
    let scrollView = NSScrollView()
    let tableView = NSTableView()
    
    private lazy var idOfRuleLabel: NSTextField = {
        let idOfRuleLabel = NSTextField()
        idOfRuleLabel.stringValue = "ID of Rule: "
        idOfRuleLabel.drawsBackground = false
        idOfRuleLabel.isEditable = false
        idOfRuleLabel.isBordered = false
        return idOfRuleLabel
    }()
    
    private lazy var idOfRuleTextField: NSTextField = {
        let idOfRuleTextField = NSTextField()
        idOfRuleTextField.backgroundColor = .darkGray
        idOfRuleTextField.bezelStyle = .roundedBezel
        idOfRuleTextField.isBordered = true
        idOfRuleTextField.placeholderString = "Enter the ID for your rule"
        return idOfRuleTextField
    }()
    
    private lazy var nameOfRuleLabel: NSTextField = {
        let nameTitle = NSTextField()
        nameTitle.stringValue = "Name of Rule: "
        nameTitle.drawsBackground = false
        nameTitle.isEditable = false
        nameTitle.isBordered = false
        return nameTitle
    }()
    
    private lazy var nameOfRuleTextField: NSTextField = {
        let nameField = NSTextField()
        nameField.backgroundColor = .darkGray
        nameField.bezelStyle = .roundedBezel
        nameField.isBordered = true
        nameField.placeholderString = "Enter the name for your rule"
        return nameField
    }()
    
    private lazy var descriptionOfRuleLabel: NSTextField = {
        let descriptionTitle = NSTextField()
        descriptionTitle.stringValue = "Description of Rule: "
        descriptionTitle.drawsBackground = false
        descriptionTitle.isEditable = false
        descriptionTitle.isBordered = false
        return descriptionTitle
    }()
    
    private lazy var descriptionOfRuleTextField: NSTextField = {
        let descriptionTextField = NSTextField()
        descriptionTextField.backgroundColor = .darkGray
        descriptionTextField.bezelStyle = .roundedBezel
        descriptionTextField.isBordered = true
        descriptionTextField.placeholderString = "Enter the description for your rule"
        return descriptionTextField
    }()
    
    private lazy var regexLabel: NSTextField = {
        let goodExampleLabel = NSTextField()
        goodExampleLabel.stringValue = "The regex for your rule: "
        goodExampleLabel.drawsBackground = false
        goodExampleLabel.isEditable = false
        goodExampleLabel.isBordered = false
        return goodExampleLabel
    }()
    
    private lazy var regexTextField: NSTextField = {
        let googdExampleTextField = NSTextField()
        googdExampleTextField.isBordered = true
        googdExampleTextField.backgroundColor = .darkGray
        googdExampleTextField.bezelStyle = .roundedBezel
        googdExampleTextField.placeholderString = "Enter the regex to searching the errors"
        return googdExampleTextField
    }()
    
    private lazy var severityLabel: NSTextField = {
        let badExampleLabel = NSTextField()
        badExampleLabel.stringValue = "The severity of your rule: "
        badExampleLabel.drawsBackground = false
        badExampleLabel.isEditable = false
        badExampleLabel.isBordered = false
        return badExampleLabel
    }()
    
    private lazy var severityTextField: NSTextField = {
        let badExampleTextField = NSTextField()
        badExampleTextField.backgroundColor = .darkGray
        badExampleTextField.bezelStyle = .roundedBezel
        badExampleTextField.isBordered = true
        badExampleTextField.placeholderString = "Enter severity for your rule. Example: warning, error"
        return badExampleTextField
    }()
    
    private lazy var addRuleButton: NSButton = {
        let button = NSButton()
        button.title = "Add Rule"
        button.target = self
        button.action = #selector(addRuleTapped)
        button.bezelStyle = .rounded
        return button
    }()
    
    private lazy var makeLintingButton: NSButton = {
        let button = NSButton()
        button.title = "Make Linting"
        button.target = self
        button.action = #selector(makeLintingTapped)
        button.bezelStyle = .rounded
        return button
    }()
    
    private lazy var backButton: NSButton = {
        let button = NSButton()
        button.title = "Back"
        button.target = self
        button.action = #selector(backButtonTapped)
        button.bezelStyle = .texturedSquare
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    init(dataStore: XcodeDatabase) {
        self.dataSource = dataStore
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cyrcle
    
    override func loadView() {
        self.view = NSView(frame: NSRect(x: 0, y: 0, width: 1000, height: 1000))
        title = "Linter creater"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pathToProject = lintWorker.getProgectFolder(path: dataSource.path)
        createFileIfNeed()
        checkPrevData()
        setupViews()
    }
    
    private func checkPrevData() {
        rulesDataSource = lintWorker.getAllRules()
        tableView.reloadData()
    }
    
    private func createFileIfNeed() {
        lintWorker.createSwiftlintConfigFile(atPath: pathToProject)
    }
    
    func setupViews() {
        view.addSubview(backButton)
        
        backButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            backButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            backButton.heightAnchor.constraint(equalToConstant: CGFloat(30)),
            backButton.widthAnchor.constraint(equalToConstant: CGFloat(50))
        ])
        
        view.addSubview(idOfRuleLabel)
        idOfRuleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            idOfRuleLabel.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: 20),
            idOfRuleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            idOfRuleLabel.widthAnchor.constraint(equalToConstant: CGFloat(150))
        ])
        
        view.addSubview(idOfRuleTextField)
        idOfRuleTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            idOfRuleTextField.centerYAnchor.constraint(equalTo: idOfRuleLabel.centerYAnchor),
            idOfRuleTextField.leadingAnchor.constraint(equalTo: idOfRuleLabel.trailingAnchor, constant: 20),
            idOfRuleTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        view.addSubview(nameOfRuleLabel)
        nameOfRuleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameOfRuleLabel.topAnchor.constraint(equalTo: idOfRuleLabel.bottomAnchor, constant: 20),
            nameOfRuleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            nameOfRuleLabel.widthAnchor.constraint(equalToConstant: CGFloat(150))
        ])
        
        view.addSubview(nameOfRuleTextField)
        nameOfRuleTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameOfRuleTextField.centerYAnchor.constraint(equalTo: nameOfRuleLabel.centerYAnchor),
            nameOfRuleTextField.leadingAnchor.constraint(equalTo: nameOfRuleLabel.trailingAnchor, constant: 20),
            nameOfRuleTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        view.addSubview(descriptionOfRuleLabel)
        descriptionOfRuleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionOfRuleLabel.topAnchor.constraint(equalTo: nameOfRuleLabel.bottomAnchor, constant: 20),
            descriptionOfRuleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            descriptionOfRuleLabel.widthAnchor.constraint(equalToConstant: CGFloat(150))
        ])
        
        view.addSubview(descriptionOfRuleTextField)
        descriptionOfRuleTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionOfRuleTextField.centerYAnchor.constraint(equalTo: descriptionOfRuleLabel.centerYAnchor),
            descriptionOfRuleTextField.leadingAnchor.constraint(equalTo: descriptionOfRuleLabel.trailingAnchor, constant: 20),
            descriptionOfRuleTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        view.addSubview(regexLabel)
        regexLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            regexLabel.topAnchor.constraint(equalTo: descriptionOfRuleLabel.bottomAnchor, constant: 20),
            regexLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            regexLabel.widthAnchor.constraint(equalToConstant: CGFloat(150))
        ])
        
        view.addSubview(regexTextField)
        regexTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            regexTextField.topAnchor.constraint(equalTo: regexLabel.topAnchor),
            regexTextField.leadingAnchor.constraint(equalTo: regexLabel.trailingAnchor, constant: 20),
            regexTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            regexTextField.heightAnchor.constraint(equalToConstant: CGFloat(110))
        ])
        
        view.addSubview(severityLabel)
        severityLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            severityLabel.topAnchor.constraint(equalTo: regexTextField.bottomAnchor, constant: 20),
            severityLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            severityLabel.widthAnchor.constraint(equalToConstant: CGFloat(150))
        ])
        
        view.addSubview(severityTextField)
        severityTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            severityTextField.topAnchor.constraint(equalTo: severityLabel.topAnchor),
            severityTextField.leadingAnchor.constraint(equalTo: severityLabel.trailingAnchor, constant: 20),
            severityTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            severityTextField.heightAnchor.constraint(equalToConstant: CGFloat(110))
        ])
        
        view.addSubview(addRuleButton)
        addRuleButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            addRuleButton.topAnchor.constraint(equalTo: severityTextField.bottomAnchor, constant: 20),
            addRuleButton.heightAnchor.constraint(equalToConstant: CGFloat(40)),
            addRuleButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            addRuleButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)

        ])
        
        view.addSubview(makeLintingButton)
        makeLintingButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            makeLintingButton.topAnchor.constraint(equalTo: addRuleButton.bottomAnchor, constant: 1),
            makeLintingButton.heightAnchor.constraint(equalToConstant: CGFloat(40)),
            makeLintingButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            makeLintingButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
    
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: makeLintingButton.bottomAnchor, constant: 20),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20)
        ])
        
        

        tableView.frame = scrollView.bounds
        tableView.delegate = self
        tableView.dataSource = self
        let customHeader = CustomTableHeaderView(frame: NSRect(x: 0, y: 0, width: 968, height: 30))
        tableView.headerView = customHeader
        scrollView.backgroundColor = NSColor.clear

        let col = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: "col"))
        col.minWidth = 350
        col.title = ""
        tableView.addTableColumn(col)

        scrollView.documentView = tableView
        scrollView.hasHorizontalScroller = false
        scrollView.hasVerticalScroller = true
    }
    
    private func checkDataOnAdd() {
        var rule: Rule?
        
        if !idOfRuleTextField.stringValue.isEmpty,
           !nameOfRuleTextField.stringValue.isEmpty,
           !regexTextField.stringValue.isEmpty,
           !severityTextField.stringValue.isEmpty {
            
            rule = Rule(id: idOfRuleTextField.stringValue,
                        name: nameOfRuleTextField.stringValue,
                        message: descriptionOfRuleTextField.stringValue.isEmpty ? nil : descriptionOfRuleTextField.stringValue,
                        regex: regexTextField.stringValue,
                        severity: severityTextField.stringValue)
        }
        
        guard let rule = rule else { return }
        
        lintWorker.addRule(rule: rule, rulemin: RuleMin(name: rule.name,
                                                        message: rule.name,
                                                        regex: rule.regex,
                                                        severity: rule.severity))
    }
    
    
    
    // MARK: - Actions
    
    @objc func backButtonTapped() {
        dismiss(self)
    }
    
    @objc func addRuleTapped() {
        checkDataOnAdd()
        rulesDataSource = lintWorker.getAllRules()
        tableView.reloadData()
    }
    
    @objc func makeLintingTapped() {
        let viewController = LintWorningsViewController()
        lintWorker.makeLintFile(completion: { data in
            viewController.dataSource = data
            self.presentAsModalWindow(viewController)
        })
    }
}

// MARK: - NSTableViewDelegate, NSTableViewDataSource

extension LintWorkerViewController: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        guard !rulesDataSource.isEmpty else { return 1 }
        return rulesDataSource.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

        if !rulesDataSource.isEmpty {
            let cell = NSTableCellView()

            let lineLabel = NSTextField()
            lineLabel.stringValue = rulesDataSource[row].name
            lineLabel.isEditable = false
            lineLabel.drawsBackground = false
            lineLabel.isBordered = false

            let warningTypeLabel = NSTextField()
            warningTypeLabel.stringValue = rulesDataSource[row].severity
            warningTypeLabel.drawsBackground = false
            warningTypeLabel.isEditable = false
            warningTypeLabel.isBordered = false

            let warningMessageLabel = NSTextField()
            warningMessageLabel.stringValue = rulesDataSource[row].regex
            warningMessageLabel.drawsBackground = false
            warningTypeLabel.isEditable = false
            warningMessageLabel.isBordered = false


            cell.addSubview(lineLabel)
            lineLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                lineLabel.centerYAnchor.constraint(equalTo: cell.centerYAnchor),
                lineLabel.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 8),
                lineLabel.widthAnchor.constraint(equalToConstant: CGFloat(300))
            ])

            cell.addSubview(warningTypeLabel)
            warningTypeLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                warningTypeLabel.centerYAnchor.constraint(equalTo: cell.centerYAnchor),
                warningTypeLabel.leadingAnchor.constraint(equalTo: lineLabel.trailingAnchor, constant: 16),
                warningTypeLabel.widthAnchor.constraint(equalToConstant: CGFloat(300))
            ])

            cell.addSubview(warningMessageLabel)
            warningMessageLabel.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                warningMessageLabel.centerYAnchor.constraint(equalTo: cell.centerYAnchor),
                warningMessageLabel.leadingAnchor.constraint(equalTo: warningTypeLabel.trailingAnchor, constant: 16),
                warningMessageLabel.widthAnchor.constraint(equalToConstant: CGFloat(300))
            ])

            return cell
        } else {
            let text = NSTextField()
            text.stringValue = "No any rules"
            let cell = NSTableCellView()
            cell.addSubview(text)
            text.drawsBackground = false
            text.isBordered = false
            text.translatesAutoresizingMaskIntoConstraints = false
            cell.addConstraint(NSLayoutConstraint(item: text, attribute: .centerY, relatedBy: .equal, toItem: cell, attribute: .centerY, multiplier: 1, constant: 0))
            cell.addConstraint(NSLayoutConstraint(item: text, attribute: .left, relatedBy: .equal, toItem: cell, attribute: .left, multiplier: 1, constant: 13))
            cell.addConstraint(NSLayoutConstraint(item: text, attribute: .right, relatedBy: .equal, toItem: cell, attribute: .right, multiplier: 1, constant: -13))
            return cell
        }
    }
    
    func tableView(_ tableView: NSTableView, rowViewForRow row: Int) -> NSTableRowView? {
        let rowView = NSTableRowView()
        rowView.isEmphasized = false
        return rowView
    }
}

class CustomTableHeaderView: NSTableHeaderView {
    let textField1 = NSTextField()
    let textField2 = NSTextField()
    let textField3 = NSTextField()

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }

    private func setupSubviews() {
        textField1.stringValue = "Name"
        textField2.stringValue = "Severity"
        textField3.stringValue = "Rule"

        textField1.isEditable = false
        textField2.isEditable = false
        textField3.isEditable = false
        textField1.drawsBackground = false
        textField1.isBordered = false
        textField2.drawsBackground = false
        textField2.isBordered = false
        textField3.drawsBackground = false
        textField3.isBordered = false

        // Размещение текстовых полей в headerView
        let stackView = NSStackView(views: [textField1, textField2, textField3])
        stackView.orientation = .horizontal
        stackView.alignment = .centerY
        stackView.spacing = 300

        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

