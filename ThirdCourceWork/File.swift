//
//  File.swift
//  ThirdCourceWork
//
//  Created by Семен Соколов
//

import Foundation

struct File {
    let date: Date
    let url: URL
}
