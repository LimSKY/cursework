//
//  RuleModel.swift
//  ThirdCourceWork
//
//  Created by Семен Соколов
//

import Foundation

struct Rule: Codable, Equatable {
    let id: String
    let name: String
    let message: String?
    let regex: String
    let severity: String
}

struct RuleMin: Codable, Equatable {
    let name: String
    let message: String?
    let regex: String
    let severity: String
}
